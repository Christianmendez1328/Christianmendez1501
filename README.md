# ![Hi](docs/hello.svg)

![cloud ENGINEER (4)](https://github.com/Christianmendez1501/Christianmendez1501/assets/143126480/75245f3f-a445-497c-bfc3-aa764d484202)





¡Hola, explorador del código! 👋 Este es mi rincón de experimentación, donde me divierto jugando con las últimas tecnologías y explorando todas las maravillas que Git, GitHub Actions, Docker, Google Functions y más tienen para ofrecer.

## Herramientas con las que trabajo que me ayudan a redescubrir este universo del coding


<a href="https://www.javascript.com/"><img src="https://img.shields.io/badge/-JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black" height="60" alt="JavaScript"></a>
<a href="https://nodejs.org/"><img src="https://img.shields.io/badge/-Node.js-339933?style=for-the-badge&logo=node.js&logoColor=white" height="60" alt="Node.js"></a>
<a href="https://reactjs.org/"><img src="https://img.shields.io/badge/-React-61DAFB?style=for-the-badge&logo=react&logoColor=white" height="60" alt="React"></a>
<a href="https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5"><img src="https://img.shields.io/badge/-HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white" height="60" alt="HTML5"></a>
<a href="https://styled-components.com/"><img src="https://img.shields.io/badge/-Styled_Components-DB7093?style=for-the-badge&logo=styled-components&logoColor=white" height="60" alt="Styled Components"></a>
<a href="https://www.python.org/"><img src="https://img.shields.io/badge/-Python-3776AB?style=for-the-badge&logo=python&logoColor=white" height="60" alt="Python"></a>
<a href="https://www.mongodb.com/"><img src="https://img.shields.io/badge/-MongoDB-47A248?style=for-the-badge&logo=mongodb&logoColor=white" height="60" alt="MongoDB"></a>
<a href="https://www.postgresql.org/"><img src="https://img.shields.io/badge/-PostgreSQL-336791?style=for-the-badge&logo=postgresql&logoColor=white" height="60" alt="PostgreSQL"></a>
<a href="https://www.docker.com/"><img src="https://img.shields.io/badge/-Docker-2496ED?style=for-the-badge&logo=docker&logoColor=white" height="60" alt="Docker"></a>
<a href="https://aws.amazon.com/"><img src="https://img.shields.io/badge/Amazon_AWS-232F3E?style=for-the-badge&logo=amazon-aws&logoColor=white" height="60" alt="Amazon AWS"></a>
<a href="https://azure.microsoft.com/"><img src="https://img.shields.io/badge/Microsoft_Azure-0089D6?style=for-the-badge&logo=microsoft-azure&logoColor=white" height="60" alt="Microsoft Azure"></a>
<a href="https://git-scm.com/"><img src="https://img.shields.io/badge/-Git-F05032?style=for-the-badge&logo=git&logoColor=white" height="60" alt="Git"></a>
<a href="https://github.com/"><img src="https://img.shields.io/badge/-GitHub-181717?style=for-the-badge&logo=github&logoColor=white" height="60" alt="GitHub"></a>
<a href="https://cloud.google.com/"><img src="https://img.shields.io/badge/Google_Cloud-4285F4?style=for-the-badge&logo=google-cloud&logoColor=white" height="60" alt="Google Cloud"></a>
<a href="https://www.terraform.io/"><img src="https://img.shields.io/badge/Terraform-623CE4?style=for-the-badge&logo=terraform&logoColor=white" height="60" alt="Terraform"></a>
<a href="https://kubernetes.io/"><img src="https://img.shields.io/badge/Kubernetes-326CE5?style=for-the-badge&logo=kubernetes&logoColor=white" height="60" alt="Kubernetes"></a>
<a href="https://www.ansible.com/"><img src="https://img.shields.io/badge/Ansible-EE0000?style=for-the-badge&logo=ansible&logoColor=white" height="60" alt="Ansible"></a>



## 🛠️ Lo que encontrarás aquí:

- **[![GitHub](https://img.shields.io/badge/-GitHub-181717?style=for-the-badge&logo=github&logoColor=white)](https://github.com/):** Explora los diversos flujos de CI/CD en archivos `.yml`. Desde saludos automáticos hasta despliegues continuos, ¡hay mucho por descubrir!

- **[![Docker](https://img.shields.io/badge/-Docker-2496ED?style=for-the-badge&logo=docker&logoColor=white)](https://www.docker.com/):** Sumérgete en el fascinante mundo de los contenedores. Encuentra Dockerfiles y archivos de configuración para desplegar aplicaciones de manera rápida y eficiente.

- **[![Google Cloud Platform](https://img.shields.io/badge/Google_Cloud_Platform-4285F4?style=for-the-badge&logo=google-cloud&logoColor=white)](https://cloud.google.com/)
:** Descubre cómo implementar y ejecutar funciones en la nube. ¡La magia de la computación sin servidor te espera!

- **Scripts y Configuraciones Diversas:** Desde simples scripts en 
[![Python](https://img.shields.io/badge/-Python-3776AB?style=for-the-badge&logo=python&logoColor=white)](https://www.python.org/) hasta archivos de configuración en [![YAML](https://img.shields.io/badge/YAML-%23FFDAB9.svg?style=for-the-badge&logo=data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMCIgaGVpZ2h0PSIxMCIgdmlld0JveD0iMCAwIDEwIDEwIj4KICA8cGF0aCBkPSJNIDQuNzY1LjQ1N0MuOTc1LjQ1NyAwIDQuNTUyLjk3NSA0Ljc2MyAwbDQuNzYyIDQuNzYyLTQuNzY0IDQuNzYzYy0uOTc1LS45NzUtMS45NzUtLjk3NS0zLjk3NCAwLS45NzUtLjk3NS0uOTc1LTMuOTc1IDAtLjk3NSAxLjk3NS0uOTc1IDMuOTc1czEuOTc1Ljk3NSAzLjk3NSAuOTc1Ljk3NSAzLjk3NXoiLz4KPC9zdmc+Cg==)](https://yaml.org/)
, hay un poco de todo para que te entretengas.

## 🚀 ¡Empecemos!

Este repositorio es un espacio de aprendizaje y experimentación. Siéntete libre de explorar, clonar y jugar con el código. Si tienes alguna pregunta, sugerencia o simplemente quieres charlar sobre tecnología, ¡no dudes en abrir un issue o un pull request!

¡Diviértete explorando y programando! 🎉

# Mi estadísticas de GitHub 📊

Algunas estadísticas interesantes sobre mi actividad en GitHub:

# ![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=Christianmendez1501&layout=compact) 

[![Christian's GitHub stats](https://github-readme-stats.vercel.app/api?username=Christianmendez1501&show_icons=true&theme=radical)](https://github.com/Christianmendez1501)






## Conectemos

[![Linkedin Badge](https://www.vectorlogo.zone/logos/linkedin/linkedin-icon.svg)](https://www.linkedin.com/in/christiandavidmendez/)

